package HelloWorld;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor; 


@Data //getter, setter, tohash, equals, to string
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor

public class Hotel {
	
	private int id;
	@NonNull
	private String name;
	private int postcode;
	private String city;
	
	
	
}
