package HelloWorld;

import java.util.ArrayList;

public class Hotel1 {
	private static ArrayList <Hotel1> hotels = new ArrayList <>();
	
	private String name;
	private String city;
	
	
	public Hotel1(String name, String city) {
		this.name = name;
		this.city = city;
		hotels.add(this);
	}
	

	@Override
	public String toString() {
		return "Hotel1 [name=" + name + ", city=" + city + "]";
	}

	public String toCSV () {
		return name + ", " + city;
	}


	public static ArrayList<Hotel1> getHotels() {
		return hotels;
	}


	public static void setHotels(ArrayList<Hotel1> hotels) {
		Hotel1.hotels = hotels;
	}
	
	
	
}
