package HelloWorld;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class ReadingMain {

	public static void main(String[] args) throws IOException {
		
		String path = "./src/HelloWorld/MOCK_DATA (2).csv";
		File f = new File(path);
		Scanner scan = new Scanner(f);
		
		while(scan.hasNext()) {
			System.out.println(scan.nextLine()); //just one line
			
			String [] parts = scan.nextLine().split(",");
			System.out.println(Arrays.toString(parts)); //to make the line into parts
			
			String companyName = parts[0]; 
			String companyCity = parts[1]; //give the parts a name and separate them
			
			//System.out.println(companyCity + "-" + companyName);
			
			new Hotel1(companyName, companyCity);

		}
		
		Hotel1.getHotels().forEach(System.out::println);
		
		File f1 = new File("./src/HelloWorld/Hotel1.java");
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))){
			for(Hotel1 hotel : Hotel1.getHotels()) {
				writer.write(hotel.toCSV() + "\n");
			}
		}catch (Exception e) {
			
		}
		
		//writer.close();
	}

}
